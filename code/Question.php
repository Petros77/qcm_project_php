<?php
class Question {
	public $idQuestion;
	public $texteQuestion;
	public $auteur;
	public $theme;
	public $reponseCorrect;
	public $reponses;
}

class Reponse {
	public $idQuestion;
	public $idReponse;
	public $texte;
}

?>