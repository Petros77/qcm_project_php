<?php
require_once('ThemeDAO.php');

class CreerThemeLogique {
	public $succes;
	
	function creerTheme($designation) {
		$dao =    new ThemeDAO();
		$theme =  new Theme();
		
		$theme->designation = $designation;
		$theme->createur    = intval($_SESSION['Personne']->idPersonne);
		$dao->creerThemeDAO($theme);
		$this->succes = true;
		
	}
}

?>