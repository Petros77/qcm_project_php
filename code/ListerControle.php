<?php
require_once('Controle.php');
require_once('ThemeLogique.php');

class ListerControle extends Controle {
	
	function executer() {
		$logique = new ThemeLogique();
		$liste = $logique->listeThemes();
		global $vue;
		$vue['listeThemes'] = $liste;
	}
}

?>