<?php
require_once('QcmDAO.php');
require_once('ContientDAO.php');

class CreerQcmLogique {
	public $succes;
	public $message;
	public $idQcm;
	
	function creerQcm($designation, $createur) {
		$dao = new QcmDAO();
		$qcm = new Qcm();
		$qcm->designation = $designation;
		$qcm->createur    = $createur;
		$dao->creer($qcm);
		$this->idQcm = $qcm->idQcm;
		$this->succes = true;
	}
	
	function creeContenuQcm($idQcm, $idQuestion) {
		$dao = new ContientDAO();
		$qcmContenu = new Contient();
		$qcmContenu->idQcm      = $idQcm;
		$qcmContenu->idQuestion = $idQuestion;
		$dao->creer($qcmContenu);
		$this->succes = true;
	}
}

?>