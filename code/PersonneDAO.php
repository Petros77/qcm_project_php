<?php
require_once('Personne.php');
require_once('CodesBDD.php');

class PersonneDAO {
	
	function lirePersonneParId($id) {
		global $DB_INFO;
		$db = null;
		$db = $DB_INFO->creeConnexion();
		$st = $db->prepare("SELECT * FROM PERSONNE WHERE idPersonne = :id");
		$st->bindParam(":id", $id);
		$st->execute();
		$resultat = $st->fetchObject('Personne');
		
		if ($resulat) {
			return $resultat;
		} else {
			return false;
		}
	}   // fin function lirePersonneParId
	
	function lirePersonne($email) {
		global $DB_INFO;
		$db = null;
		$db = $DB_INFO->creeConnexion();
		$st = $db->prepare("SELECT * FROM personne WHERE email= :email");
		$st->bindParam(":email", $email, PDO::PARAM_STR);
		$st->execute();
		$resultat = $st->fetchObject('Personne');
		$resultat->idPersonne = intval($resultat->idPersonne);
		if ($resultat) {
			return $resultat;
		} else {
			return false;
		}
	}
}
?>