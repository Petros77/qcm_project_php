<?php
require_once('CodesBDD.php');
require_once('Personne.php');
require_once('PersonneDAO.php');

class ConnexionLogique {
	public $personne;
	
	function connecterPersonne($email, $motDePasse) {
		global $DB_INFO;
		$personne = null;
		$dao = new PersonneDAO();
		$pers = $dao->lirePersonne($email);
		if ($pers != null && $pers->motDePasse === $motDePasse) {
			$this->personne = $pers;
			$this->personne->motDePasse = null;
		}
	}
	
	function connexionCorrecte() {
		return $this->personne != null;
	}
}
?>