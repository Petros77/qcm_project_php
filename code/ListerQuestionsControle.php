<?php
require_once('Controle.php');
require_once('QuestionLogique.php');

class ListerQuestionsControle extends Controle {
	public $page = "listerQuestions.php";
	
	function executer() {
		$logique = new QuestionLogique();
		$liste = $logique->listeQuestions();
		global $vue;
		$vue['listeQuestions'] = $liste;
	}
}

?>