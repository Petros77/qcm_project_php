<?php
require_once('ReponseDAO.php');

class CreerReponseLogique {
	public $succes;
	public $idReponse;
	
	function creerReponse($idQuestion, $texte) {
		$dao = new ReponseDAO();
		$reponse = new Reponse();
		$reponse->idQuestion = intval($idQuestion);
		$reponse->texte      = $texte;
		$dao->creer($reponse);
		$this->idReponse = $reponse->idReponse;
		$this->succes = true;
		
	}
}

?>